package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import java.io.IOException;
import java.util.List;
import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

public class Class2 {

	public WebDriver driver;
	
@BeforeMethod
@Parameters("browser")
public void beforeTest(String browser) {
	if (browser.equals("firefox")) {
		System.setProperty("webdriver.gecko.driver", "c://browseri/firefoxdriver.exe");
		driver = new FirefoxDriver();
	} else {
		System.setProperty("webdriver.chrome.driver", "c://browseri/chromedriver.exe");
		driver = new ChromeDriver();
	}
}
		
@AfterMethod
public void afterTest() {
		driver.quit();	  
}	

@Test
public void main() throws InterruptedException, IOException {

		// open Lufthansa
		driver.get("http://www.lufthansa.com/hr/en/Homepage");
		Thread.sleep(3000);
		driver.findElement(By.id("flightmanagerFlightsFormOrigin")).sendKeys("JFK");
		Thread.sleep(1000);
		driver.findElement(By.id("flightmanagerFlightsFormOrigin")).sendKeys(Keys.TAB);
		Thread.sleep(1000);
		driver.findElement(By.id("flightmanagerFlightsFormDestination")).sendKeys("SPU");
		Thread.sleep(1000);
		driver.findElement(By.id("flightmanagerFlightsFormDestination")).sendKeys(Keys.TAB);
		Thread.sleep(1000);
		driver.findElement(By.id("flightmanagerFlightsFormOutboundDateDisplay")).click();
		Thread.sleep(1000);
		driver.findElement(By.className("next")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@aria-label='Th, 05.01.2017']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@aria-label='Th, 12.01.2017']")).click();
		Thread.sleep(1000);
		Select dropdownAdults = new Select(driver.findElement(By.id("flightmanagerFlightsFormAdults")));
		Thread.sleep(1000);
		dropdownAdults.selectByVisibleText("2 Adults ");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[contains(text(),'Search flights')]")).click();
		Thread.sleep(3000);
		List<WebElement> cheapestDays = driver.findElements(By.className("cheapest-day"));
		for (WebElement element : cheapestDays) {
			if (element.getAttribute("alt").equals("Lowest fare on this day")) {
				System.out.println("Lowest Fare is ok");
			}
		}
		driver.quit();
}

}
