package automationFramework;

import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import java.io.File;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;



public class Class1 {
	
	public WebDriver driver;
	
@BeforeMethod
@Parameters("browser")
public void beforeTest(String browser) {
	if (browser.equals("firefox")) {
		System.setProperty("webdriver.gecko.driver", "c://browseri/firefoxdriver.exe");
		driver = new FirefoxDriver();
	} else {
		System.setProperty("webdriver.chrome.driver", "c://browseri/chromedriver.exe");
		driver = new ChromeDriver();
	}
}
	
@AfterMethod
public void afterTest() {
		driver.quit();	  
}

@Test(priority=1)
public void main() throws InterruptedException, BiffException, IOException {
		driver.get("http://demoqa.com/");
		
		//go to registration and wait 1 second
		driver.findElement(By.id("menu-item-374")).click();
		Thread.sleep(1000);
		
		//load the .xls file with user data
		Workbook wBook = Workbook.getWorkbook(new File("c://noviGoal/goal111216/src/userBase.xls"));
		jxl.Sheet Sheet = wBook.getSheet(0); 
		
		//add First name, Last name
		driver.findElement(By.id("name_3_firstname")).sendKeys(Sheet.getCell(0, 1).getContents());
		driver.findElement(By.id("name_3_lastname")).sendKeys(Sheet.getCell(1, 1).getContents());
		
		//add marital status by using xpath
		driver.findElement(By.xpath("//input[@value='" + Sheet.getCell(2, 1).getContents() + "']")).click();
		
		//add hobbies (only existing ones)
		for(int i=3; i<6; i++){
			if (Sheet.getCell(i, 1).getContents() != ""){
				driver.findElement(By.xpath("//input[@value='" + Sheet.getCell(i, 1).getContents() + "']")).click();
			}
		}
		
		// add Country, DOB (Month, Day, Year)
		Select dropdownCountry = new Select(driver.findElement(By.id("dropdown_7")));
		dropdownCountry.selectByVisibleText(Sheet.getCell(6, 1).getContents());
		Select dropdownMonth = new Select(driver.findElement(By.id("mm_date_8")));
		dropdownMonth.selectByVisibleText(Sheet.getCell(7, 1).getContents());
		Select dropdownDay = new Select(driver.findElement(By.id("dd_date_8")));
		dropdownDay.selectByVisibleText(Sheet.getCell(8, 1).getContents());
		Select dropdownYear = new Select(driver.findElement(By.id("yy_date_8")));
		dropdownYear.selectByVisibleText(Sheet.getCell(9, 1).getContents());
		
		// add Phone Number, user name, E-mail, password and confirm password
		driver.findElement(By.id("phone_9")).sendKeys(Sheet.getCell(10, 1).getContents());
		driver.findElement(By.id("username")).sendKeys(Sheet.getCell(11, 1).getContents());
		driver.findElement(By.id("email_1")).sendKeys(Sheet.getCell(12, 1).getContents());
		driver.findElement(By.id("description")).sendKeys(Sheet.getCell(13, 1).getContents());
		driver.findElement(By.id("password_2")).sendKeys(Sheet.getCell(14, 1).getContents());
		driver.findElement(By.id("confirm_password_password_2")).sendKeys(Sheet.getCell(14, 1).getContents());
		
		// wait 3 seconds and submit
		//Thread.sleep(3000);
		//driver.findElement(By.name("pie_submit")).click();
		
		// wait 3 seconds and click on 'Frames and Windows', followed by clicking on 'Open Seprate New Window'
		// and then on 'Open New Seprate Window'
		Thread.sleep(3000);
		driver.findElement(By.linkText("Frames and windows")).click();
		driver.findElement(By.id("ui-id-2")).click();
		String winHandleBefore = driver.getWindowHandle();  // define the current window
		driver.findElement(By.linkText("Open New Seprate Window")).click();
		
		// switch to newly opened window
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		// Click on menu button, close the popup by pressing the 'X' and then press 'HOME'
		Thread.sleep(2000);
		driver.findElement(By.className("dt-mobile-menu-icon")).click();
		Thread.sleep(1000);
		driver.findElement(By.className("ib-img-default")).click();
		Thread.sleep(2000);
		driver.findElement(By.linkText("HOME")).click();
		
		// wait a few seconds and then close the windows and go back to the previous one
		Thread.sleep(3000);
		driver.close();
		driver.switchTo().window(winHandleBefore);
		Thread.sleep(3000);
}
	
}
